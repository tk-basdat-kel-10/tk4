from django import forms
from django.db import connection

class CreateRuanganForm(forms.Form):
    def __init__(self, kode_choices=None, *args, **kwargs):
        super(CreateRuanganForm, self).__init__(*args, **kwargs)
        self.fields['kode'].choices = kode_choices

    kode = forms.ChoiceField(label='Kode:', required=True, choices=())

class PasienForm(forms.Form):
    def __init__(self, kode_choices=None, *args, **kwargs):
        super(PasienForm, self).__init__(*args, **kwargs)
        self.fields['kode'].choices = kode_choices

    kode = forms.ChoiceField(label='kode', required=True, choices=())

class ChooseRsForm(forms.Form):
    def __init__(self, kode_choices=None, *args, **kwargs):
        super(ChooseRsForm, self).__init__(*args, **kwargs)
        self.fields['kode'].choices = kode_choices

    kode = forms.ChoiceField(label='Kode:', required=True, choices=())
