from django.urls import path
from . import views

app_name = "trigger2"

urlpatterns = [
    path('createjadwalDokter/', views.createJadwalDokter, name='createJadwalDokter'),
    path('createjadwal/<str:kode_faskes>/<str:shift>/<str:tanggal>', views.createJadwal, name='createJadwal'),
    path('listjadwalDokter/', views.readJadwalDokter, name='readJadwalDokter'),
    path('listappointment/', views.listAppointment, name='listAppointment'),
    path('listruanganRS/', views.listRuanganRS, name='listRuanganRS'),
    path('updateruanganRS/<str:koders>/<str:koderuangan>', views.updateRuanganRS, name='updateRuanganRS'),
    path('listbedRS/', views.listBedRs, name='listBedRs'),
    path('deletebedRS/<str:koders>/<str:koderuangan>/<str:kodebed>', views.deleteBedRS, name='deleteBedRS'),
    path('updateappointment/<str:nik_pasien>/<str:nostr>/<str:username_dokter>/<str:kode_faskes>/<str:praktek_shift>/<str:praktek_tgl>', views.updateappointment, name='updateappointment'),
    path('deleteappointment/<str:nik_pasien>/<str:nostr>/<str:username_dokter>/<str:kode_faskes>/<str:praktek_shift>/<str:praktek_tgl>', views.deleteappointment, name='deleteappointment'),
    path('createBedRS/', views.createBedRS, name='createBedRS'),
    path('createRuanganRS/', views.createRuanganRS, name='createRuanganRS'),
    path('createAppointment/', views.createappointment, name='createappointment'),
    path('buatappointment/<str:nostr>/<str:username_dokter>/<str:kode_faskes>/<str:praktek_shift>/<str:praktek_tgl>', views.buatappointment, name='buatappointment'),
    path('ruang/<str:koders>', views.ruang, name='ruang'),
    path('createBedRS/', views.createBedRS, name='createBedRS'),
    path('bed/', views.createbed, name='createbed'),
    path('bedX/', views.bed, name='bed'),
    path('hasilruang/<str:koders>/<str:koderuangan>', views.hasilruang, name='hasilruang'),
]