$(document).ready(function () {
    function getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                const cookie = cookies[i].trim();
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken');

    $('select#rskode').on('change', function(){
        var koders = $("#rskode option:selected").val();
        console.log(koders)
        $.ajax({
            type: 'POST',
            headers: {
                "X-CSRFToken": csrftoken,
            },
            url: '/bed/',
            data: {'koders' : koders},
            success: function(data){

                var kodeRuangans = data.listofKodeRuang;
                console.log(kodeRuangans)
                $('select#koderuangan').empty();   
                $('select#koderuangan').append('<option> pilih ruangan rs </option>');

                for (var i=0; i< kodeRuangans.length; i++){
                    var htmlbaru = '<option value=' + kodeRuangans[i] + '>' + kodeRuangans[i] + '</option>';

                    $('select#koderuangan').append(htmlbaru);
                }
            }
        })  

    $('select#koderuangan').on('change', function(){
        var kode = $("#koderuangan option:selected").val();
        console.log(kode)
        $.ajax({
            type: 'POST',
            headers: {
                "X-CSRFToken": csrftoken,
            },
            url: '/bedX/',
            data: {'kode' : kode},
            success: function(data){
                console.log(data)
                $("input[name='kode_bed']").val(data);
                $("input[name='custId']").val(data);
            }
        })
    })

    })
});