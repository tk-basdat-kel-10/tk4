from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.sessions.models import Session
from django.db import connection
from .forms import CreateRuanganForm, PasienForm, ChooseRsForm
from django.db.utils import InternalError, IntegrityError
from django.contrib import messages


# Create your views here.

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

#nomor 4
def createJadwalDokter(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM jadwal")
        row = dictfetchall(cursor)
        context = {
            'jadwal': row,
        }
        return render(request, 'createJadwalDokter.html', context)

def createJadwal(request, kode_faskes, shift, tanggal):
    email = request.session['email']
    orang = request.session['type']
    with connection.cursor() as cursor:
        #select dokter sesuai nama dokter yg sedang akses
        cursor.execute("SELECT username FROM dokter where username =%s",[email])
        nama = cursor.fetchone()
        n = nama[0]
        cursor.execute("SELECT nostr FROM dokter where username =%s",[email])
        str = cursor.fetchone()
        s = str[0]
        cursor.execute("select * from jadwal_dokter where nostr=%s and username=%s and kode_faskes=%s and shift=%s and tanggal=%s",[s, n, kode_faskes, shift, tanggal])
        row = cursor.fetchone()
        print(row)
        if row == None: 
            cursor.execute("INSERT INTO jadwal_dokter VALUES(%s, %s, %s, %s, %s, '0')",[s, n, kode_faskes, shift, tanggal])
            return redirect("trigger2:readJadwalDokter")
        else:
            return redirect("trigger2:readJadwalDokter")

def readJadwalDokter(request):
    email = request.session['email']
    maile = request.session['type']
    with connection.cursor() as cursor:
        if maile == 'penggunaPublik' or maile == 'adminSatgas':
            cursor.execute("SELECT * FROM jadwal_dokter")
        else:
            cursor.execute("SELECT * FROM jadwal_dokter where username =%s",[email])

        row = dictfetchall(cursor)
        context = {
            'list_jadwal': row,
        }
    return render(request, 'readJadwalDokter.html', context)

#nomor 5
def createappointment(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM jadwal_dokter")
        row = dictfetchall(cursor)
        context = {
            'jadwal': row,
        }
        return render(request, 'createAppointment.html', context)

def buatappointment(request, nostr, username_dokter, kode_faskes, praktek_tgl, praktek_shift):
    email = request.session['email']
    orang = request.session['type']
    with connection.cursor() as cursor:
        #pembuat admin satgas
        if orang == 'adminSatgas':
            cursor.execute("SELECT * FROM pasien")
        #pembuat si pengguna publik
        if orang == 'penggunaPublik':
            cursor.execute("SELECT * FROM pasien where idpendaftar =%s",[email])

        choice =  [(_[0], _[0]) for _ in cursor.fetchall()]
        cursor.execute("SELECT * FROM jadwal_dokter where nostr=%s and username=%s and kode_faskes=%s and tanggal=%s and shift=%s",[nostr, username_dokter, kode_faskes, praktek_tgl, praktek_shift])
        row = dictfetchall(cursor)

        if request.method == 'POST':
            data = request.POST.get('koders')
            try:
                cursor.execute("INSERT INTO memeriksa VALUES(%s, %s, %s, %s, %s, %s)",
                [ data, nostr, username_dokter, kode_faskes, praktek_shift, praktek_tgl])
                return redirect("trigger2:listAppointment")
            except InternalError:
                messages.add_message(request, messages.WARNING, f"Silahkan pilih shift dan tanggal lainnya, karena shift dan tanggal yang dipilih sudah penuh")
                return redirect("trigger2:createappointment")
        else:
            form = PasienForm(choice)
            context = {
                'periksa': row[0],
                'form' : form,
            }
            return render(request, 'buatAppointment.html', context)

def listAppointment(request):
    email = request.session['email']
    orang = request.session['type']
    with connection.cursor() as cursor:
        if orang == 'dokter':
            cursor.execute("SELECT * FROM memeriksa where username_dokter =%s",[email]) 
        elif orang == 'adminSatgas':
            cursor.execute("SELECT * FROM memeriksa")
        elif orang == 'penggunaPublik':
            cursor.execute("SELECT nik FROM pasien where idpendaftar=%s",[email])
            str = cursor.fetchone()
            s = str[0]
            cursor.execute("SELECT * FROM memeriksa where nik_pasien=%s",[str])
        row = dictfetchall(cursor)
        context = {
            'list_periksa': row,
        }
        return render(request, 'listAppointment.html', context)

def updateappointment(request, nik_pasien, nostr, username_dokter, kode_faskes, praktek_shift, praktek_tgl):
    if request.method == 'POST':
        with connection.cursor() as cursor:
            rekomendasi = request.POST.get('rekomendasi')
            cursor.execute("UPDATE memeriksa SET rekomendasi=%s WHERE nik_pasien=%s AND nostr=%s AND username_dokter=%s AND kode_faskes=%s AND praktek_shift=%s AND praktek_tgl=%s", [rekomendasi, nik_pasien, nostr, username_dokter, kode_faskes, praktek_shift, praktek_tgl])
            return redirect('trigger2:listAppointment')

    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM memeriksa WHERE nik_pasien=%s AND nostr=%s AND username_dokter=%s AND kode_faskes=%s AND praktek_shift=%s AND praktek_tgl=%s", [nik_pasien, nostr, username_dokter, kode_faskes, praktek_shift, praktek_tgl])
        row = dictfetchall(cursor)
        context = {
            'periksa':row[0],
        }
        return render(request, 'updateappointment.html', context)

def deleteappointment(request, nik_pasien, nostr, username_dokter, kode_faskes, praktek_shift, praktek_tgl):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM memeriksa WHERE nik_pasien=%s AND nostr=%s AND username_dokter=%s AND kode_faskes=%s AND praktek_shift=%s AND praktek_tgl=%s", [nik_pasien, nostr, username_dokter, kode_faskes, praktek_shift, praktek_tgl])
        return redirect('trigger2:listAppointment')

# nomor 6
def listRuanganRS(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM ruangan_rs")
        row = dictfetchall(cursor)
        context = {
            'list': row,
        }
    return render(request, 'listRuanganRS.html', context)

def listBedRs(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT b.koders, b.koderuangan, b.kodebed FROM bed_rs b, reservasi_rs r WHERE b.koders = r.koders AND b.koderuangan = r.koderuangan AND b.kodebed = r.kodebed AND r.tglkeluar < now()")
        tanggal = dictfetchall(cursor)
        cursor.execute("SELECT b.koders, b.koderuangan, b.kodebed FROM bed_rs b, reservasi_rs r WHERE b.koders = r.koders AND b.koderuangan = r.koderuangan AND b.kodebed = r.kodebed AND r.tglkeluar > now()")
        row = dictfetchall(cursor)
        cursor.execute("(SELECT b.koders, b.koderuangan, b.kodebed FROM bed_rs b) EXCEPT (select a.koders, a.koderuangan, a.kodebed from reservasi_rs a)")
        r = dictfetchall(cursor)
        context = {
            'list': row,
            'tgldelete':tanggal,
            'lis':r
        }
    return render(request, 'listBedRS.html', context)

def updateRuanganRS(request, koders, koderuangan):
    if request.method == 'POST':
        with connection.cursor() as cursor:
            tipe = request.POST.get('tipe')
            harga = request.POST.get('harga')
            cursor.execute("UPDATE ruangan_rs SET tipe=%s, harga=%s WHERE koders=%s AND koderuangan=%s" ,[tipe, harga, koders, koderuangan])
            return redirect("trigger2:listRuanganRS")

    with connection.cursor() as cursor:
        cursor.execute("SELECT * from ruangan_rs WHERE koders=%s AND koderuangan=%s",[koders, koderuangan])
        row = dictfetchall(cursor)
        context = {
            'ruang': row[0],
        }
    return render(request, 'updateRuanganRS.html', context)

def deleteBedRS(request, koders, koderuangan, kodebed):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM bed_rs WHERE koders=%s AND koderuangan=%s AND kodebed=%s", [koders, koderuangan, kodebed])
        return redirect('trigger2:listBedRs')

def createRuanganRS(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT kode_faskes FROM rumah_sakit")
        choice =  [(_[0], _[0]) for _ in cursor.fetchall()]
        
        if request.method == 'POST':
            data = request.POST.get('koders')
            return redirect("/ruang/" + data)

        form = CreateRuanganForm(choice)
        context = {
                'form' : form,
            }
        return render(request, 'createRuanganRS.html', context)

def ruang(request, koders):
    with connection.cursor() as cursor:
        cursor.execute(" SELECT split_part( max(koderuangan) :: TEXT, 'R', 2) FROM ruangan_rs ")
        str = cursor.fetchone()
        s = str[0]
        print(s)
        a = int(s)
        print(a)
        b = a + 1
        print(b)
        converted_num = "% s" % b
        d = len( converted_num)
        if d == 1:
            hasil = 'R0' +  converted_num
        else:
            hasil = 'R' +  converted_num

        context = {
            'koders': koders,
            'kode':hasil,
        }
        return render(request, 'ruang.html', context)

def hasilruang(request, koders, koderuangan):
    if request.method == 'POST':
        with connection.cursor() as cursor:
            tipe = request.POST.get('tipe')
            harga = request.POST.get('harga')
            cursor.execute("INSERT INTO ruangan_rs VALUES(%s, %s, %s, %s, %s)",
            [ koders, koderuangan, tipe, '0', harga])
            return redirect("trigger2:listRuanganRS")

def createBedRS(request):
    if request.method == 'POST':
        with connection.cursor() as cursor:
            rskode = request.POST.get('koders')
            rkode = request.POST.get('koderuangan')
            bedkode = request.POST.get('custId')
            print(rskode)
            print(rkode)
            print(bedkode)
            cursor.execute("INSERT INTO bed_rs VALUES(%s, %s, %s)",
            [  rkode, rskode, bedkode])
            return redirect("trigger2:listBedRs")

    with connection.cursor() as cursor:
        cursor.execute("SELECT kode_faskes FROM rumah_sakit")
        choice =  [(_[0], _[0]) for _ in cursor.fetchall()]
        form = ChooseRsForm(choice)

        context = {
            'form' : form,
        }
        return render(request, 'createBdRS.html', context)

def createbed(request):
    if request.method == 'POST':
        with connection.cursor() as cursor:
            ruangan = request.POST.get('koders')
            cursor.execute("SELECT koderuangan FROM ruangan_rs where koders=%s",[ruangan])
            koderuangan = dictfetchall(cursor)
            # print(koderuangan)
            listofKodeRuang = []
            for kode in koderuangan : 
                listofKodeRuang.append(kode["koderuangan"])
            # print(listofKodeRuang)
            return JsonResponse({'listofKodeRuang' : listofKodeRuang})  
            
    with connection.cursor() as cursor:
        cursor.execute("SELECT kode_faskes FROM rumah_sakit")
        rskode = dictfetchall(cursor)
        listofKodeRs = []
        for kode in rskode : 
            listofKodeRs.append(kode["kode_faskes"])
        # print(listofKodeRs)
        return JsonResponse({'listofKodeRs' : listofKodeRs})    

def bed(request):
    if request.method == 'POST':
        with connection.cursor() as cursor:
            bed = request.POST.get('kode')
            # print(bed)
            cursor.execute(" SELECT split_part( max(kodebed) :: TEXT, 'B', 2) FROM bed_rs where koderuangan=%s",[bed])
            str = cursor.fetchone()
            s = str[0]
            # print(s)
            if s != None:
                a = int(s)
                # print(a)
                b = a + 1
                # print(b)
                converted_num = "% s" % b
                d = len( converted_num)
                if d == 1:
                    hasil = 'B0' +  converted_num
                else:
                    hasil = 'B' +  converted_num
            else:
                hasil = 'B01'
            return JsonResponse(hasil, safe=False)  