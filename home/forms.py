from django import forms

class FormRoleSelect(forms.Form):
    ROLES = (
        (1, "Admin Sistem"),
        (2, "Pengguna Publik"),
        (3, "Dokter"),
        (4, "Admin Satgas"),
    )
    role = forms.ChoiceField(choices=ROLES)

class FormRegistAdmin(forms.Form):
	email = forms.CharField(required=True)
	password = forms.CharField(widget=forms.PasswordInput())

class FormRegistPenggunaPublik(forms.Form):
    email = forms.CharField(required=True)
    password = forms.CharField(widget=forms.PasswordInput())
    nama = forms.CharField(required=True)
    nik = forms.CharField(required=True)
    no_hp = forms.CharField(required=True)

class FormRegistDokter(forms.Form):
    email = forms.CharField(required=True)
    password = forms.CharField(widget=forms.PasswordInput())
    no_str = forms.CharField(required=True)
    nama = forms.CharField(required=True)
    no_hp = forms.CharField(required=True)
    gelar_depan = forms.CharField(required=True)
    gelar_belakang = forms.CharField(required=True)

class FormRegistSatgas(forms.Form):
    email = forms.CharField(required=True)
    password = forms.CharField(widget=forms.PasswordInput())
    id_faskes = forms.CharField(required=True)

class FormLogin(forms.Form):
	email = forms.CharField()
	password = forms.CharField(widget=forms.PasswordInput())