from django.urls import path
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.home, name='home'),
    path('index/', views.landing_page, name='landing_page'),
    path('login/', views.login, name='login'),
    path('registrole/', views.regist_role, name='regist_role'),
    path('registadmin/', views.regist_admin, name='regist_admin'),
    path('registdokter/', views.regist_dokter, name='regist_dokter'),
    path('registpenggunapublik/', views.regist_penggunapublik, name='regist_penggunapublik'),
    path('registsatgas/', views.regist_satgas, name='regist_satgas'),
    path('logout/', views.logout, name='logout'),
]
