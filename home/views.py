from django.shortcuts import render
from django.db import connection
from .forms import FormRoleSelect, FormRegistAdmin, FormRegistPenggunaPublik, FormRegistDokter, FormRegistSatgas, FormLogin
from django.shortcuts import redirect

def home(request):
    return render(request, "home.html")

def landing_page(request):
	if 'email' not in request.session:
		return redirect('home:home')
	
	email = request.session['email']
	user_type = request.session['type']

	if user_type == "unknown":
		return redirect('home:home')

	with connection.cursor() as cursor:
		cursor.execute("SELECT * FROM AKUN_PENGGUNA WHERE username=%s",[email])
		row = dictfetchall(cursor)
		if user_type == 'adminSistem':
			cursor.execute("SELECT * FROM ADMIN WHERE username=%s",[email])
			row2 = dictfetchall(cursor)
		elif user_type == 'dokter':
			cursor.execute("SELECT * FROM DOKTER WHERE username=%s",[email])
			row2 = dictfetchall(cursor)
		elif user_type == 'penggunaPublik':
			cursor.execute("SELECT * FROM PENGGUNA_PUBLIK WHERE username=%s",[email])
			row2 = dictfetchall(cursor)
		elif user_type == 'adminSatgas':
			cursor.execute("SELECT * FROM ADMIN_SATGAS WHERE username=%s",[email])
			row2 = dictfetchall(cursor)

	return render(request, "index.html",{
		'message' : "Selamat datang, " + email,
        'email': email,
        'type': user_type,
		'peran' : row[0]['peran'],
    })

def regist_role(request):
	if request.method == 'POST':
		form = FormRoleSelect(request.POST)
		if form.is_valid():
			if form.cleaned_data.get('role') == '1':
				return redirect('home:regist_admin')
			elif form.cleaned_data.get('role') == '2':
				return redirect('home:regist_penggunapublik')
			elif form.cleaned_data.get('role') == '3':
				return redirect('home:regist_dokter')
			elif form.cleaned_data.get('role') == '4':
				return redirect('home:regist_satgas')
	else:
		regist_role  = FormRoleSelect(request.POST or None)
		context = {
			"regist_role":regist_role,
		}
		return render(request,"registRole.html",context)

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def get_type(username):
	with connection.cursor() as cursor:
		cursor.execute("SELECT * FROM ADMIN_SATGAS WHERE username=%s",[username])
		row = dictfetchall(cursor)
		if len(row) == 1:
			return "adminSatgas"
	with connection.cursor() as cursor:
		cursor.execute("SELECT * FROM DOKTER WHERE username=%s",[username])
		row = dictfetchall(cursor)
		if len(row) == 1:
			return "dokter"
	with connection.cursor() as cursor:
		cursor.execute("SELECT * FROM PENGGUNA_PUBLIK WHERE username=%s",[username])
		row = dictfetchall(cursor)
		if len(row) == 1:
			return "penggunaPublik"
	with connection.cursor() as cursor:
		cursor.execute("SELECT * FROM ADMIN WHERE username=%s",[username])
		row = dictfetchall(cursor)
		if len(row) == 1:
			return "adminSistem"
	return "Unknown"

def regist_admin(request):
	request.session.flush()
	if request.method == 'POST':
		form = FormRegistAdmin(request.POST)
		if form.is_valid():
			email = form.cleaned_data.get('email')
			password = form.cleaned_data.get('password')

			if check_email_exist(email):
				message = "Maaf email sudah digunakan di akun lain"
				regist_admin  = FormRegistAdmin()
				return render(request, 'registAdmin.html', {'create_admin': regist_admin, 'message': message})

			try:
				with connection.cursor() as cursor:
					cursor.execute("INSERT INTO AKUN_PENGGUNA VALUES (%s, %s, %s)", [email, password, "ADMIN_SISTEM"])
					cursor.execute("INSERT INTO ADMIN VALUES (%s)", [email])
			except:
				regist_admin  = FormRegistAdmin()
				return render(request, 'registAdmin.html', {'create_admin': regist_admin, 'message': "Password harus minimal terdapat 1 angka dan 1 huruf kapital"})

			request.session['email'] = email
			request.session['type'] = get_type(email)
			return render(request, 'index.html', {
				'message' : "Registration Success! Selamat datang, " + email,
				'email' : email,
			})
			

	else:
		regist_admin  = FormRegistAdmin()
		context = {
			"create_admin":regist_admin,
		}
		return render(request,"registAdmin.html",context)

def regist_penggunapublik(request):
	request.session.flush()
	if request.method == 'POST':
		form = FormRegistPenggunaPublik(request.POST)
		if form.is_valid():
			email = form.cleaned_data.get('email')
			password = form.cleaned_data.get('password')
			nama = form.cleaned_data.get('nama')
			nik = form.cleaned_data.get('nik')
			no_hp = form.cleaned_data.get('no_hp')

			if check_email_exist(email):
				message = "Maaf email sudah digunakan di akun lain"
				regist_penggunapublik  = FormRegistPenggunaPublik()
				return render(request, 'registPenggunaPublik.html', {'create_penggunapublik': regist_penggunapublik, 'message': message})

			try:
				with connection.cursor() as cursor:
					cursor.execute("INSERT INTO AKUN_PENGGUNA VALUES (%s, %s, %s)", [email, password, "PENGGUNA_PUBLIK"])
					cursor.execute("INSERT INTO PENGGUNA_PUBLIK VALUES (%s, %s, %s, %s, %s, %s)", [email, nik, nama, "AKTIF", "PENGGUNA_PUBLIK", no_hp])

			except:
				regist_penggunapublik  = FormRegistPenggunaPublik()
				return render(request, 'registPenggunaPublik.html', {'create_penggunapublik': regist_penggunapublik, 'message': "Password harus minimal terdapat 1 angka dan 1 huruf kapital"})

			request.session['email'] = email
			request.session['type'] = get_type(email)
			return render(request, 'index.html', {
				'message' : "Registration Success! Selamat datang, " + email,
				'email' : email,
			})
	else:
		regist_penggunapublik  = FormRegistPenggunaPublik()
		context = {
			"create_penggunapublik":regist_penggunapublik,
		}
		return render(request,"registPenggunaPublik.html",context)

def regist_dokter(request):
	request.session.flush()
	if request.method == 'POST':
		form = FormRegistDokter(request.POST)
		if form.is_valid():
			email = form.cleaned_data.get('email')
			password = form.cleaned_data.get('password')
			no_str = form.cleaned_data.get('no_str')
			nama = form.cleaned_data.get('nama')
			no_hp = form.cleaned_data.get('no_hp')
			gelar_depan = form.cleaned_data.get('gelar_depan')
			gelar_belakang = form.cleaned_data.get('gelar_belakang')

			if check_email_exist(email):
				message = "Maaf email sudah digunakan di akun lain"
				regist_dokter  = FormRegistDokter()
				return render(request, 'registDokter.html', {'create_dokter': regist_dokter, 'message': message})

			try:
				with connection.cursor() as cursor:
					cursor.execute("INSERT INTO AKUN_PENGGUNA VALUES (%s, %s, %s)", [email, password, "DOKTER"])
					cursor.execute("INSERT INTO ADMIN VALUES (%s)", [email])
					cursor.execute("INSERT INTO DOKTER VALUES (%s, %s, %s, %s, %s, %s)", [email, no_str, nama, no_hp, gelar_depan, gelar_belakang])

			except:
				regist_dokter  = FormRegistDokter()
				return render(request, 'registDokter.html', {'create_dokter': regist_dokter, 'message': "Password harus minimal terdapat 1 angka dan 1 huruf kapital"})
			
			request.session['email'] = email
			request.session['type'] = get_type(email)
			return render(request, 'index.html', {
				'message' : "Registration Success! Selamat datang, " + email,
				'email' : email,
			})
			
	else:
		regist_dokter  = FormRegistDokter()
		context = {
			"create_dokter":regist_dokter,
		}
		return render(request,"registDokter.html", context)
	
def regist_satgas(request):
	request.session.flush()
	if request.method == 'POST':
		form = FormRegistSatgas(request.POST)
		if form.is_valid():
			email = form.cleaned_data.get('email')
			password = form.cleaned_data.get('password')
			id_faskes = form.cleaned_data.get('id_faskes')

			if check_email_exist(email):
				message = "Maaf email sudah digunakan di akun lain"
				regist_satgas  = FormRegistSatgas()
				return render(request, 'registSatgas.html', {'create_satgas': regist_satgas, 'message': message})

			try:
				with connection.cursor() as cursor:
					cursor.execute("INSERT INTO AKUN_PENGGUNA VALUES (%s, %s, %s)", [email, password, "ADMIN_SATGAS"])
					cursor.execute("INSERT INTO ADMIN VALUES (%s)", [email])
					cursor.execute("INSERT INTO ADMIN_SATGAS VALUES (%s, %s)", [email, id_faskes])

			except:
				regist_satgas  = FormRegistSatgas()
				return render(request, 'registSatgas.html', {'create_satgas': regist_satgas, 'message': "Password harus minimal terdapat 1 angka dan 1 huruf kapital"})

			request.session['email'] = email
			request.session['type'] = get_type(email)
			return render(request, 'index.html', {
				'message' : "Registration Success! Selamat datang, " + email,
				'email' : email,
			})
	
	else:
		regist_satgas  = FormRegistSatgas()
		context = {
			"create_satgas":regist_satgas,
		}
		return render(request,"registSatgas.html", context)

def login(request):
	
	if request.method == 'POST':
		form  = FormLogin(request.POST)
		if form.is_valid():
			email = form.cleaned_data.get('email')
			password = form.cleaned_data.get('password')
			with connection.cursor() as cursor:
				cursor.execute("SELECT * FROM AKUN_PENGGUNA WHERE username=%s AND password=%s",[email, password])
				row = dictfetchall(cursor)
				if len(row) == 1:
					request.session['email'] = email
					request.session['type'] = get_type(email)
					return redirect('home:landing_page')
				else:
					return render(request, 'login.html', {
      				  	'form': form,
						'message' : 'Maaf Username atau password salah'
    				})
		return render(request, 'login.html', {
			'form': form,
		})

	else:
		form  = FormLogin()
		context = {
			'form' : form
		}
		return render(request,"login.html", context)

def logout(request):
	request.session.flush()
	return redirect('home:home')

def check_email_exist(email):
	with connection.cursor() as cursor:
		cursor.execute("SELECT * FROM AKUN_PENGGUNA WHERE username = %s", [email])
		row = dictfetchall(cursor)
		if len(row) == 1 :
			return True
		return False