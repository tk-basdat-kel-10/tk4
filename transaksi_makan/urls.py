from django.urls import path
from django.contrib.auth import views
from . import views

app_name = 'transaksi_makanan'

urlpatterns = [
    path('transaksi_makan/paket_makan',views.list_paket_makan, name='paket_makan'),
    path('transaksi_makan/form_paket_makan',views.form_paket_makan, name='form_paket_makan'),
    path('transaksi_makan/delete_paket_makan/<str:kodehotel>/<str:kodepaket>', views.delete_paket_makan, name='delete_paket_makan'),
    path('transaksi_makan/<str:kodehotel>/<str:kodepaket>', views.update_paket_makan, name='update_paket_makan'),
    path('transaksi_makan/',views.list_transaksi_makan, name='transaksi_makan'),
]