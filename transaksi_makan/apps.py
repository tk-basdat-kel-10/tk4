from django.apps import AppConfig


class TransaksiMakanConfig(AppConfig):
    name = 'transaksi_makan'
