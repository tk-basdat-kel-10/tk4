from django import forms
from django.db import connection
from collections import namedtuple

class CreatePaketMakan(forms.Form):
    def get_id_hotel():
        cursor = connection.cursor()
        cursor.execute('set search_path to siruco;')
        cursor.execute('select kode from hotel;')
        response = cursor.fetchall()
        response2 = []
        for tup in response:
            response2.append((tup[0], tup[0]))
        cursor.close()

        return response2

    kode_hotel = forms.ChoiceField(label="Kode Hotel", choices= get_id_hotel())

    kode_paket2 = forms.CharField(label='Kode Paket', max_length=5, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tulis kode paket',
        'type' : 'text',
        'required': True,
    }))

    nama_paket = forms.CharField(label='Nama Paket', widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tulis kode paket',
        'type' : 'text',
        'required': True,
    }))

    harga = forms.IntegerField(label='Harga',required=True,widget=forms.NumberInput,min_value=0)



