from django.shortcuts import render
from django.http import response
from django.shortcuts import render, redirect
from django.db import connection
from .forms import CreatePaketMakan
from collections import namedtuple
# Create your views here.

def namedtuplefetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return[dict(zip(columns, row)) for row in cursor.fetchall()]

def form_paket_makan(request):
    with connection.cursor() as cursor:
        response = {}
        response['error'] = False
        form = CreatePaketMakan(request.POST or None)
        response['form'] = form
        if request.method == 'POST':
            if form.is_valid():
                kode_hotel = request.POST.get('kode_hotel')
                kode_paket = request.POST.get('kode_paket2')
                nama_paket = request.POST.get('nama_paket')
                harga = request.POST.get('harga')
                cursor.execute("set search_path to siruco;")
                cursor.execute("INSERT INTO PAKET_MAKAN (kodehotel,kodepaket,nama,harga) VALUES ('{}','{}','{}','{}');".format(kode_hotel,kode_paket,nama_paket,harga))
                return redirect('transaksi_makan:paket_makan')
    form =  CreatePaketMakan()
    args = {
        'form' : form,
    }
    return render(request, 'form_paket_makan.html',args)

def list_paket_makan(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute('SELECT KodeHotel, KodePaket, nama, harga FROM PAKET_MAKAN;')
    list_paket_makan = cursor.fetchall()

    cursor.close()

    with connection.cursor() as cursor:
        cursor.execute("SELECT kodehotel, kodepaket, nama, harga FROM PAKET_MAKAN")
        all_paket_makan = namedtuplefetchall(cursor)
        kode_hotel = all_paket_makan[0]["kodehotel"]
        kode_paket =  all_paket_makan[0]["kodepaket"]
        nama =  all_paket_makan[0]["nama"]
        harga =  all_paket_makan[0]["harga"]

        args = {
            'list_paket_makan':list_paket_makan,
            'kode_hotel':kode_hotel,
            'kode_paket':kode_paket,
            'nama':nama,
            'harga':harga,
        }
        return render(request, 'paket_makan.html', args)

def delete_paket_makan(request,kodehotel,kodepaket):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM PAKET_MAKAN WHERE kodehotel ='{}' AND kodepaket ='{}';".format(kodehotel,kodepaket))
        return redirect('transaksi_makanan:paket_makan') 
    
def update_paket_makan(request,kodehotel,kodepaket):
    if request.method == 'POST':
        with connection.cursor() as cursor:
            nama = request.POST.get('nama')
            harga = request.POST.get('harga')
            cursor.execute("UPDATE PAKET_MAKAN SET kodeHotel ='{}', kodePaket ='{}', nama ='{}', harga ='{}' WHERE kodeHotel ='{}' AND kodePaket ='{}';".format(kodehotel,kodepaket,nama,harga,kodehotel,kodepaket))
            return redirect('transaksi_makan:paket_makan')

    with connection.cursor() as cursor:
        cursor.execute("SELECT * from PAKET_MAKAN WHERE kodehotel='{}' AND kodepaket='{}'".format(kodehotel, kodepaket))
        row = namedtuplefetchall(cursor)
        args = {
            'paket': row[0],
        }
    return render(request, 'update_paket_makan.html', args)

def list_transaksi_makan(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute('SELECT idtransaksi, idtransaksimakan, totalbayar FROM TRANSAKSI_MAKAN;')
    list_transaksi_makan = cursor.fetchall()

    cursor.close()

    with connection.cursor() as cursor:
        cursor.execute("SELECT idtransaksi, idtransaksimakan, totalbayar FROM TRANSAKSI_MAKAN;")
        all_transaksi_makan = namedtuplefetchall(cursor)
        idtransaksi = all_transaksi_makan[0]["idtransaksi"]
        idtransaksimakan =  all_transaksi_makan[0]["idtransaksimakan"]
        totalbayar =  all_transaksi_makan[0]["totalbayar"]

        args = {
            'list_transaksi_makan':list_transaksi_makan,
            'idtransaksi':idtransaksi,
            'idtransaksimakan':idtransaksimakan,
            'totalbayar':totalbayar,
        }
        return render(request, 'transaksi_makan.html', args)

