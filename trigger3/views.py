from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.sessions.models import Session
from django.db import connection
from django.db.utils import InternalError, IntegrityError
from django.contrib import messages


# Create your views here.

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def readReservasiRS(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute('SELECT kodepasien, tglmasuk, tglkeluar, koders, koderuangan, kodebed FROM RESERVASI_RS;')
    readReservasiRS = cursor.fetchall()

    cursor.close()

    with connection.cursor() as cursor:
        cursor.execute("SELECT kodepasien, tglmasuk, tglkeluar, koders, koderuangan, kodebed FROM RESERVASI_RS;")
        all_reservasi_rs = dictfetchall(cursor)
        kodepasien = all_reservasi_rs[0]["kodepasien"]
        tglmasuk =  all_reservasi_rs[0]["tglmasuk"]
        tglkeluar =  all_reservasi_rs[0]["tglkeluar"]
        koders =  all_reservasi_rs[0]["koders"]
        koderuangan =  all_reservasi_rs[0]["koderuangan"]
        kodebed =  all_reservasi_rs[0]["kodebed"]

        args = {
            'readReservasiRS':readReservasiRS,
            'kodepasien':kodepasien,
            'tglmasuk':tglmasuk,
            'tglkeluar':tglkeluar,
            'koders':koders,
            'koderuangan':koderuangan,
            'kodebed':kodebed,
        }
        return render(request, 'readReservasiRS.html', args)

def readFaskes(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute('SELECT kode, tipe, nama FROM FASKES;')
    readFaskes = cursor.fetchall()

    cursor.close()

    with connection.cursor() as cursor:
        cursor.execute("SELECT kode, tipe, nama FROM FASKES;")
        all_faskes = dictfetchall(cursor)
        kode = all_faskes[0]["kode"]
        tipe =  all_faskes[0]["tipe"]
        nama =  all_faskes[0]["nama"]

        args = {
            'readFaskes':readFaskes,
            'kode':kode,
            'tipe':tipe,
            'nama':nama,
        }
        return render(request, 'readFaskes.html', args)

def readJadwal(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute('SELECT kode_faskes, shift, tanggal FROM jadwal;')
    readJadwal = cursor.fetchall()

    cursor.close()

    with connection.cursor() as cursor:
        cursor.execute("SELECT kode_faskes, shift, tanggal FROM jadwal;")
        all_jadwal = dictfetchall(cursor)
        kode_faskes = all_jadwal[0]["kode_faskes"]
        shift =  all_jadwal[0]["shift"]
        tanggal =  all_jadwal[0]["tanggal"]

        args = {
            'readJadwal':readJadwal,
            'kode_faskes':kode_faskes,
            'shift':shift,
            'tanggal':tanggal,
        }
        return render(request, 'readJadwal.html', args)


def readTransaksirs(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIRUCO;')
    cursor.execute('SELECT IdTransaksi, KodePasien, TanggalPembayaran, WaktuPembayaran, TglMasuk, TotalBiaya, StatusBayar FROM TRANSAKSI_RS;')
    readTransaksirs = cursor.fetchall()

    cursor.close()

    with connection.cursor() as cursor:
        cursor.execute("SELECT IdTransaksi, KodePasien, TanggalPembayaran, WaktuPembayaran, TglMasuk, TotalBiaya, StatusBayar FROM TRANSAKSI_RS;")
        all_transaksirs = dictfetchall(cursor)
        IdTransaksi = all_transaksirs[0]["IdTransaksi"]
        KodePasien =  all_transaksirs[0]["KodePasien"]
        TanggalPembayaran = all_transaksirs[0]["TanggalPembayaran"]
        WaktuPembayaran = all_transaksirs[0]["WaktuPembayaran"]
        TglMasuk = all_transaksirs[0]["TglMasuk"]
        TotalBiaya = all_transaksirs[0]["TotalBiaya"]
        StatusBayar = all_transaksirs[0]["StatusBayar"]

        args = {
            'readTransaksirs':readTransaksirs,
            'IdTransaksi':IdTransaksi,
            'KodePasien':KodePasien,
            'TanggalPembayaran':TanggalPembayaran,
            'WaktuPembayaran':WaktuPembayaran,
            'TglMasuk':TglMasuk,
            'TotalBiaya':TotalBiaya,
            'StatusBayar':StatusBayar,
        }
        return render(request, 'readJadwal.html', args)
