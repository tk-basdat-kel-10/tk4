from django.urls import path
from . import views

app_name = 'trigger3'

urlpatterns = [
    path('readReservasiRS/', views.readReservasiRS, name='readReservasiRS'),
    path('readFaskes/', views.readFaskes, name='readFaskes'),
    path('readJadwal/', views.readJadwal, name='readJadwal'),
]
