from django import forms
from django.db import connection

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

class FormCreateRuanganHotel(forms.Form):

    cursor = connection.cursor()
    cursor.execute("SELECT DISTINCT kodehotel FROM HOTEL_ROOM")
    list_kode_hotel = dictfetchall(cursor)
    cursor.execute("SELECT * FROM HOTEL_ROOM order by koderoom desc")
    no_ruangan = int(dictfetchall(cursor)[0]['koderoom'][2:])

    choices = []

    i = 0
    for i in range(len(list_kode_hotel)):
        choices.append((list_kode_hotel[i]['kodehotel'], list_kode_hotel[i]['kodehotel']))
    
    tuple(choices)

    kode_hotel = forms.ChoiceField(choices=choices)
    kode_ruangan = forms.CharField(initial='RM' + str(no_ruangan+1)+' (disabled)', widget = forms.TextInput(attrs={'readonly':'readonly'}))
    jenis_bed = forms.CharField(required=True)
    tipe = forms.CharField(required=True)
    harga = forms.IntegerField(required=True)

class FormUpdateRuanganHotel(forms.Form):

    jenis_bed = forms.CharField(required=True)
    tipe = forms.CharField(required=True)
    harga = forms.IntegerField(required=True)

class FormCreateReservasiHotel(forms.Form):

    cursor = connection.cursor()
    cursor.execute("SELECT nik FROM PASIEN")
    list_nik_pasien = dictfetchall(cursor)

    choices = []

    i = 0
    for i in range(len(list_nik_pasien)):
        choices.append((list_nik_pasien[i]['nik'], list_nik_pasien[i]['nik']))

    tuple(choices)

    nik_pasien = forms.ChoiceField(choices=choices)
    tgl_masuk = forms.DateTimeField(required=True)
    tgl_keluar = forms.DateTimeField(required=True)
    kode_hotel = ""