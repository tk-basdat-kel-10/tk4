from django.shortcuts import render
from django.db import connection
from .forms import FormCreateRuanganHotel, FormUpdateRuanganHotel
from django.shortcuts import redirect

# Create your views here.

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def create_ruangan_hotel(request):
    if request.method == 'POST':
        form = FormCreateRuanganHotel(request.POST)
        if form.is_valid():
            with connection.cursor() as cursor:
                cursor.execute("SELECT * FROM HOTEL_ROOM order by koderoom desc")
                no_ruangan = int(dictfetchall(cursor)[0]['koderoom'][2:])
            kode_hotel = form.cleaned_data.get('kode_hotel')
            kode_ruangan = 'RM' + str(no_ruangan+1)
            jenis_bed = form.cleaned_data.get('jenis_bed')
            tipe = form.cleaned_data.get('tipe')
            harga = form.cleaned_data.get('harga')

            with connection.cursor() as cursor:
                cursor.execute("INSERT INTO HOTEL_ROOM VALUES (%s, %s, %s, %s, %s)", [kode_hotel, kode_ruangan, jenis_bed, tipe, str(harga)])
            
            message = "Ruangan hotel " + kode_ruangan + " berhasil dibuat!"
            
            return render(request, 'createRuanganHotel.html', {
                'message' : message,
                "create_ruangan_hotel" : form,
                })
    else:
        form = FormCreateRuanganHotel()
        context = {
            "create_ruangan_hotel" : form
        }
        return render(request, "createRuanganHotel.html", context)

def read_ruangan_hotel(request):
    if request.method == 'GET':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM HOTEL_ROOM")
            row = dictfetchall(cursor)
        
        return render(request, "readRuanganHotel.html", {
            'row' : row,
        })

def update_ruangan_hotel(request, koderoom):
    print("h")

    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM HOTEL_ROOM WHERE koderoom = %s", [koderoom])
        data = dictfetchall(cursor)

    if request.method == 'POST':
        form = FormUpdateRuanganHotel(request.POST)
        if form.is_valid():
            jenis_bed = form.cleaned_data.get('jenis_bed')
            tipe = form.cleaned_data.get('tipe')
            harga = form.cleaned_data.get('harga')

            with connection.cursor() as cursor:
                cursor.execute("UPDATE HOTEL_ROOM SET jenisbed = %s, tipe = %s, harga = %s WHERE koderoom=%s", [jenis_bed, tipe, harga, koderoom])

            return render(request, "updateRuanganHotel.html", {
                'data' : data[0],
                'form' : form,
            })

    else:
        print("s")
        form = FormUpdateRuanganHotel()
        context = {
            'data' : data[0],
            'form' : form,
        }
        print("a")
        return render(request, "updateRuanganHotel.html", context)

def delete_ruangan_hotel(request, koderoom):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM RESERVASI_HOTEL WHERE koderoom=%s",[koderoom])
        cursor.execute("DELETE FROM HOTEL_ROOM WHERE koderoom=%s",[koderoom])
    return redirect('trigger4:read_ruangan_hotel')


def create_reservasi_hotel(request):
    if request.method == 'POST':
        form = FormCreateRuanganHotel(request.POST)
        if form.is_valid():
            with connection.cursor() as cursor:
                cursor.execute("SELECT * FROM HOTEL_ROOM order by koderoom desc")
                no_ruangan = int(dictfetchall(cursor)[0]['koderoom'][2:])
            kode_hotel = form.cleaned_data.get('kode_hotel')
            kode_ruangan = 'RM' + str(no_ruangan+1)
            jenis_bed = form.cleaned_data.get('jenis_bed')
            tipe = form.cleaned_data.get('tipe')
            harga = form.cleaned_data.get('harga')

            with connection.cursor() as cursor:
                cursor.execute("INSERT INTO HOTEL_ROOM VALUES (%s, %s, %s, %s, %s)", [kode_hotel, kode_ruangan, jenis_bed, tipe, str(harga)])
            
            message = "Ruangan hotel " + kode_ruangan + " berhasil dibuat!"
            
            return render(request, 'createRuanganHotel.html', {
                'message' : message,
                "create_ruangan_hotel" : form,
                })
    else:
        form = FormCreateRuanganHotel()
        context = {
            "create_ruangan_hotel" : form
        }
        return render(request, "createRuanganHotel.html", context)

def read_reservasi_hotel(request):
    if request.method == 'GET':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM RESERVASI_HOTEL")
            row = dictfetchall(cursor)
        
        return render(request, "readReservasiHotel.html", {
            'row' : row,
        })

def read_transaksi_hotel(request):
    if request.method == 'GET':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM TRANSAKSI_HOTEL")
            row = dictfetchall(cursor)
        
        return render(request, "readTransaksiHotel.html", {
            'row' : row,
        })