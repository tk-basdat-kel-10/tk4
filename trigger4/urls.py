from django.urls import path
from . import views

app_name = 'trigger4'

urlpatterns = [
    path('create-ruangan-hotel/', views.create_ruangan_hotel, name='create_ruangan_hotel'),
    path('read-ruangan-hotel/', views.read_ruangan_hotel, name='read_ruangan_hotel'),
    path('update-ruangan-hotel/<str:koderoom>', views.update_ruangan_hotel, name='update_ruangan_hotel'),
    path('delete-ruangan-hotel/<str:koderoom>', views.delete_ruangan_hotel, name='delete_ruangan_hotel'),
    path('create-reservasi-hotel/', views.create_reservasi_hotel, name='create_reservasi_hotel'),
    path('read-reservasi-hotel/', views.read_reservasi_hotel, name='read_reservasi_hotel'),
    path('read-transaksi-hotel/', views.read_transaksi_hotel, name='read_transaksi_hotel'),
]