from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.sessions.models import Session
from django.db import connection
from django.db.utils import InternalError
from django.contrib import messages


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def daftar(request):
    email = request.session['email']
    if request.method == 'POST':
        with connection.cursor() as cursor:
            nik = request.POST.get('nik')
            idpendaftar = email
            nama = request.POST.get('nama')
            kJalan = request.POST.get('ktp_jalan') 
            kKelurahan = request.POST.get('ktp_kelurahan')
            kKecamatan = request.POST.get('ktp_kecamatan')
            kKabKota = request.POST.get('ktp_kabkot')
            kProvinsi = request.POST.get('ktp_prov')
            dJalan = request.POST.get('dom_jalan')         
            dKelurahan = request.POST.get('dom_kelurahan')     
            dKecamatan = request.POST.get('dom_kecamatan')     
            dKabKota = request.POST.get('domk') 
            dProvinsi  = request.POST.get('dom_prov')  
            notelp = request.POST.get('notelp')
            nohp = request.POST.get('nohp')
            cursor.execute("SELECT nik FROM pasien WHERE nik=%s",[nik])
            a = cursor.fetchone()
            print(a)
            if a == None:
                cursor.execute("INSERT INTO pasien VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", 
                [nik, idpendaftar, nama, kJalan, kKelurahan, kKecamatan, kKabKota, kProvinsi, dJalan, dKelurahan, dKecamatan, dKabKota, dProvinsi, notelp, nohp])
                return redirect('pasien:read')
            else:
                messages.add_message(request, messages.WARNING, f"Pasien dengan nik tersebut telah terdaftar")
                return redirect("pasien:daftar")   
    context = {
        'email':email,
    }
    return render(request, 'daftar.html', context)

def read(request):
    email = request.session['email']
    maile = request.session['type']
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM pasien where idpendaftar =%s",[email])
        row = dictfetchall(cursor)
        context = {
            'lis': row,
        }
    return render(request, 'read.html', context)

def detail(request, nik):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM pasien where nik=%s",[nik])

        row = dictfetchall(cursor)
        context = {
            'row': row,
        }
    return render(request, 'detail.html', context)


def delete(request, nik):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM pasien where nik=%s",[nik])
        return redirect('pasien:read')

def update(request, nik):
    email = request.session['email']
    if request.method == 'POST':
        with connection.cursor() as cursor:
            nik = nik
            idpendaftar = email
            nama = request.POST.get('nama')
            kJalan = request.POST.get('ktp_jalan') 
            kKelurahan = request.POST.get('ktp_kelurahan')
            kKecamatan = request.POST.get('ktp_kecamatan')
            kKabKota = request.POST.get('ktp_kabkot')
            kProvinsi = request.POST.get('ktp_prov')
            dJalan = request.POST.get('dom_jalan')         
            dKelurahan = request.POST.get('dom_kelurahan')     
            dKecamatan = request.POST.get('dom_kecamatan')     
            dKabKota = request.POST.get('domk') 
            dProvinsi  = request.POST.get('dom_prov')  
            notelp = request.POST.get('notelp')
            nohp = request.POST.get('nohp') 
            cursor.execute("UPDATE pasien SET ktp_jalan=%s WHERE nik=%s", [kJalan, nik])
            cursor.execute("UPDATE pasien SET ktp_kelurahan=%s WHERE nik=%s", [kKelurahan, nik])
            cursor.execute("UPDATE pasien SET ktp_kecamatan=%s WHERE nik=%s", [kKecamatan, nik])
            cursor.execute("UPDATE pasien SET ktp_kabkot=%s WHERE nik=%s", [kKabKota, nik])
            cursor.execute("UPDATE pasien SET ktp_prov=%s WHERE nik=%s", [kProvinsi, nik])
            cursor.execute("UPDATE pasien SET dom_jalan=%s WHERE nik=%s", [dJalan, nik])
            cursor.execute("UPDATE pasien SET dom_kelurahan=%s WHERE nik=%s", [dKelurahan, nik])
            cursor.execute("UPDATE pasien SET dom_kecamatan=%s WHERE nik=%s", [dKecamatan, nik])
            cursor.execute("UPDATE pasien SET dom_kabkot=%s WHERE nik=%s", [dKabKota, nik])
            cursor.execute("UPDATE pasien SET dom_prov=%s WHERE nik=%s", [dProvinsi, nik])
            cursor.execute("UPDATE pasien SET notelp=%s WHERE nik=%s", [notelp, nik])
            cursor.execute("UPDATE pasien SET nohp=%s WHERE nik=%s", [nohp, nik])
            return redirect('pasien:read')

    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM pasien where nik=%s",[nik])
        row = dictfetchall(cursor)
        context = {
            'periksa':row[0],
            'email':email,
        }
        return render(request, 'update.html', context)