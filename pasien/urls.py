from django.urls import path
from . import views

app_name = "pasien"

urlpatterns = [
    path('daftar/', views.daftar, name='daftar'),
    path('read/', views.read, name='read'),
    path('detail/<str:nik>', views.detail, name='detail'),
    path('delete/<str:nik>', views.delete, name='delete'),
    path('update/<str:nik>', views.update, name='update'),
]